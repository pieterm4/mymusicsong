﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Music.Helper;
using Music.Repository.Interface;
using SQLite.Net.Async;

namespace Music.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class, new()
    {
        public SQLiteAsyncConnection Context { get; set; }

        public GenericRepository(IDbConnection conn)
        {
            Context = conn.GetAsyncConnection();
        }
        public AsyncTableQuery<T> AsQueryable()
        {
            return Context.Table<T>();
        }

        public async Task<int> Delete(T entity)
        {
            return await Context.DeleteAsync(entity);
        }

        public async Task<List<T>> Get()
        {
            return await Context.Table<T>().ToListAsync();
        }

        public async Task<T> Get(Expression<Func<T, bool>> predicate)
        {
            return await Context.FindAsync<T>(predicate);
        }

        public async Task<T> Get(int id)
        {
            return await Context.FindAsync<T>(id);
        }

        public async Task<List<T>> Get<TValue>(Expression<Func<T, bool>> predicate = null, Expression<Func<T, TValue>> orderBy = null)
        {
            var query = Context.Table<T>();
            if (predicate != null)
            {
                query = query.Where(predicate);
            }
            if (orderBy != null)
            {
                query = query.OrderBy<TValue>(orderBy);
            }
            return await query.ToListAsync();
        }

        public async Task<int> Insert(T entity)
        {
            return await Context.InsertAsync(entity);
        }

        public async Task<int> Update(T entity)
        {
            return await Context.DeleteAsync(entity);
        }
    }

   
}
