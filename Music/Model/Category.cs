﻿using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;

namespace Music.Model
{
    [Table("Category")]
    public class Category
    {
        [AutoIncrement, Key, PrimaryKey]
        public int IdCategory { get; set; }
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
