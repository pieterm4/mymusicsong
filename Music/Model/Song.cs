﻿using System.ComponentModel.DataAnnotations;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;


namespace Music.Model
{
    [System.ComponentModel.DataAnnotations.Schema.Table("Song")]
    public class Song
    {
        [Key, AutoIncrement, PrimaryKey]
        public int Id { get; set; }

        [Display(Name = "Title")]
        public string Title { get; set; }

        [Display(Name = "Author")]
        public string Author { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Display(Name = "Text")]
        public string Text { get; set; }
        [ManyToOne]
        public virtual Category Category { get; set; }

        public override string ToString()
        {
            return Title + "\n\r" + Author;
        }
    }
}
