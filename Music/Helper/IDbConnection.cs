﻿using System.Threading.Tasks;
using SQLite.Net.Async;

namespace Music.Helper
{
    public interface IDbConnection
    {
        Task InitializeDatabase();
        SQLiteAsyncConnection GetAsyncConnection();
    }
}
