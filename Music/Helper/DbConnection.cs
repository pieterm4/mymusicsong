﻿using System;
using System.IO;
using System.Threading.Tasks;
using Music.Model;
using SQLite.Net;
using SQLite.Net.Async;
using SQLite.Net.Platform.WinRT;

namespace Music.Helper
{
    public class DbConnection:IDbConnection
    {
        private readonly string dbPath;
        private SQLiteAsyncConnection conn;

        public DbConnection()
        {
            dbPath = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "MyDatabase.sqlite");
            var connectionFactory = new Func<SQLiteConnectionWithLock>(() => new SQLiteConnectionWithLock(new SQLitePlatformWinRT(), new SQLiteConnectionString(dbPath, storeDateTimeAsTicks: false)));
            conn = new SQLiteAsyncConnection(connectionFactory);
        }
        public async Task InitializeDatabase()
        {
            await conn.CreateTableAsync<Category>();
            await conn.CreateTableAsync<Song>();
        }

        public SQLiteAsyncConnection GetAsyncConnection()
        {
            return conn;
        }

       
    }
}
