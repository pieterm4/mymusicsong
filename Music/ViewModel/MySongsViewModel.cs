﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Windows.UI.Xaml;
using Music.Model;
using Music.Repository;

namespace Music.ViewModel
{
    public class MySongsViewModel:ViewModelBase
    {
        public ObservableCollection<Song> Songs
        {
            get { return _songs; }
            set
            {
                if (Equals(value, _songs)) return;
                _songs = value;
                OnPropertyChanged(nameof(Songs));
            }
        }

        public ObservableCollection<Category> Categories
        {
            get { return _categories; }
            set
            {
                if (Equals(value, _categories)) return;
                _categories = value;
                OnPropertyChanged(nameof(Categories));
            }
        }

        private readonly GenericRepository<Category> CategoryRepository;
        private readonly GenericRepository<Song> SongRepository;

        private Music.App app = (Application.Current as App);
        private ObservableCollection<Song> _songs;
        private ObservableCollection<Category> _categories;

        public MySongsViewModel()
        {
            CategoryRepository = new GenericRepository<Category>(app.ODbConnection);
            SongRepository = new GenericRepository<Song>(app.ODbConnection);
            GetSongs();

            
        }

        public async void GetSongs()
        {
            List<Song> Songes = await  SongRepository.Get();
            Songs = new ObservableCollection<Song>(Songes);
            List<Category> categories = await CategoryRepository.Get();
            Categories = new ObservableCollection<Category>(categories);
        }
    }
}