﻿using System;
using System.Collections.Generic;
using System.Linq;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Music.Model;
using Music.Repository;
using Music.ViewModel.Command;

namespace Music.ViewModel
{
    public class AddSongViewModel:ViewModelBase
    {
        private int _id;
        private string _title;
        private string _author;
        private string _description;
        private string _text;
        private Category _category;

        public int Id
        {
            get { return _id; }
            set
            {
                if (value == _id) return;
                _id = value;
                OnPropertyChanged(nameof(Id));
            }
        }

        public string Title
        {
            get { return _title; }
            set
            {
                if (value == _title) return;
                _title = value;
                OnPropertyChanged(nameof(Title));
            }
        }

        public string Author
        {
            get { return _author; }
            set
            {
                if (value == _author) return;
                _author = value;
                OnPropertyChanged(nameof(Author));
            }
        }

        public string Description
        {
            get { return _description; }
            set
            {
                if (value == _description) return;
                _description = value;
                OnPropertyChanged(nameof(Description));
            }
        }

        public string Text
        {
            get { return _text; }
            set
            {
                if (value == _text) return;
                _text = value;
                OnPropertyChanged(nameof(Text));
            }
        }

        public virtual Category Category
        {
            get { return _category; }
            set
            {
                if (Equals(value, _category)) return;
                _category = value;
                OnPropertyChanged(nameof(Category));
            }
        }

        private readonly GenericRepository<Category> CategoryRepository;
        private readonly GenericRepository<Song> SongRepository;
        private Music.App app = (Application.Current as App);
        private List<Category> _categories;
        private int _ilepoltonow;

        //Commands
        public CommandBase AddSong { get; set; }
        public CommandBase Transpose { get; set; }


        public AddSongViewModel()
        {
            CategoryRepository = new GenericRepository<Category>(app.ODbConnection);
            SongRepository = new GenericRepository<Song>(app.ODbConnection); 
            AddSong = new CommandBase(AddSongCommand, CanAddSongExecute);
            Transpose = new CommandBase(TransposeCommand, CanTransposeExecute);
            InitializeCategories();
        }

       

        private bool CanAddSongExecute(object obj)
        {
            return true;
        }

        public List<Category> Categories
        {
            get { return _categories; }
            set
            {
                if (Equals(value, _categories)) return;
                _categories = value;
                OnPropertyChanged(nameof(Categories));
            }
        }

        public async  void InitializeCategories()
        {
            var categories = new List<Category>();
            categories = await CategoryRepository.Get();
            Categories = categories;
        }

        private async void AddSongCommand(object obj)
        {
            try
            {
                var song = new Song()
                {
                    Category = Category,
                    Author = Author,
                    Description = Description,
                    Text = Text,
                    Title = Text
                };

                if (song != null && Category != null && Author != null && Text != null)
                {
                    await SongRepository.Insert(song);
                }
                else
                {
                    MessageDialog msg = new MessageDialog("Musisz uzupełnić dane", "Uwaga");
                    await msg.ShowAsync();
                }
            }
            catch (Exception ex)
            {
                
                MessageDialog msg = new MessageDialog("Coś poszło nie tak: \n" + ex.ToString(), "Uwaga, błąd");
                await msg.ShowAsync();
            }

                
         }

        //Transponowanie
        public int ilepoltonow
        {
            get { return _ilepoltonow; }
            set
            {
                if (value == _ilepoltonow) return;
                _ilepoltonow = value;
                OnPropertyChanged(nameof(ilepoltonow));
            }
        }

        private bool CanTransposeExecute(object obj)
        {
            return true;
        }

        private void TransposeCommand(object obj)
        {
            throw new NotImplementedException();
        }
    }
}
