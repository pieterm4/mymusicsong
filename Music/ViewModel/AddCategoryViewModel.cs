﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Music.Model;
using Music.Repository;
using Music.ViewModel.Command;
using SQLite.Net.Async;
using SQLite.Net.Attributes;

namespace Music.ViewModel
{
    public class AddCategoryViewModel:ViewModelBase
    {
        private string _name;
        private string _description;
        

        [AutoIncrement, Key, PrimaryKey]
        public int IdCategory { get; set; } = 0;

        public string Name
        {
            get { return _name; }
            set
            {
                if (value == _name) return;
                _name = value;
                AddCategory.OnCanExecuteChanged();
                OnPropertyChanged(nameof(Name));
                
            }
        }

        public string Description
        {
            get { return _description; }
            set
            {
                if (value == _description) return;
                _description = value;
                OnPropertyChanged(nameof(Description));
            }
        }

  

        public CommandBase AddCategory { get; set; }
        public CommandBase CancelCategory { get; set; }

        private readonly GenericRepository<Category> CategoryRepository;
        public AddCategoryViewModel()
        {
            AddCategory = new CommandBase(AddCategoryCommand, CanAddCategoryExecute);
            
           CategoryRepository = new GenericRepository<Category>(app.ODbConnection);
        }

       


        private bool CanAddCategoryExecute(object obj)
        {
            if (!string.IsNullOrEmpty(Name))
            {
                return true;
            }
            else
                return false;
        }

        private Music.App app = (Application.Current as App);
        private SQLiteAsyncConnection conn;
        private async void AddCategoryCommand(object obj)
        {
            try
            {
                var category = new Category()
                {
                    IdCategory = IdCategory,
                    Description = Description,
                    Name = Name,

                };

                if (category != null && !string.IsNullOrEmpty(Description) && !string.IsNullOrEmpty(Name))
                {
                    await CategoryRepository.Insert(category);
                }
                else
                {
                    MessageDialog msg = new MessageDialog("Musisz uzupełnić dane", "Uwaga");
                    await msg.ShowAsync();
                }
                
            }
            catch (Exception ex)
            {

                MessageDialog msg = new MessageDialog("Coś poszło nie tak: \n" + ex.ToString(), "Uwaga, błąd");
                await msg.ShowAsync();
            }
         

            //var lista = await CategoryRepository.Get();
            //var cat = lista.FirstOrDefault();
            //MessageDialog msg = new MessageDialog(cat.ToString(), "Ciekawe");
            //await msg.ShowAsync();
        }
    }
}
